// Ken Vicchiollo - Part 1
// AJ Vetter - Part 2
// Lab Exercise #2 - Playing Cards
// September 8, 2020 - September 10, 2020

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Suit
{
	Club, 
	Diamond,
	Heart,
	Spade
};

enum Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

struct Card
{
	Rank Rank;
	Suit Suit;
};

void PrintCard(Card card)
{
	string text = "The ";

	switch ((int)card.Rank)
	{
		case 2: text += "2"; break;
		case 3: text += "3"; break;
		case 4: text += "4"; break;
		case 5: text += "5"; break;
		case 6: text += "6"; break;
		case 7: text += "7"; break;
		case 8: text += "8"; break;
		case 9: text += "9"; break;
		case 10: text += "10"; break;
		case 11: text += "Jack"; break;
		case 12: text += "Queen"; break;
		case 13: text += "King"; break;
		case 14: text += "Ace"; break;
	}

	text += " of ";

	switch (card.Suit)
	{
		case Club: text += "Clubs"; break;
		case Diamond: text += "Diamonds"; break;
		case Heart: text += "Heart"; break;
		case Spade: text += "Spades"; break;
	}

	cout << text;
}

Card HighCard(Card card1, Card card2)
{
	if ((int)card1.Rank > (int)card2.Rank) return card1;
	else return card2;
} 

int main()
{


	(void)_getch();
	return 0;
}